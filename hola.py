from bs4 import BeautifulSoup
from urllib2 import urlopen
import codecs
import json

url = 'http://www.cecyt9.ipn.mx/Paginas/inicio.aspx'
html = urlopen(url).read()
soup = BeautifulSoup(html, 'lxml')
newsBox = soup.findAll('div', {'class': 'ca-item-main'})

news = {'news': []}

for idx, notice in enumerate(newsBox):
    if notice.find('div', {'class': 'ca-text'}):
        noticeTitle = notice.find('div', {'class': 'ca-text'}).find('span').text.replace('\t', '').replace('\r', '').replace('\n', '')
        noticeImage = notice.find('div', {'class': 'ca-icon'}).find('img')['src']
        print noticeImage
        if noticeTitle[0] == ' ':
            noticeTitle = noticeTitle[1:]
            noticeTitle = noticeTitle.upper()
            noticeLink = notice.find('a', {'class': 'ca-more'})['href']
            noticeID = idx + 1
            noticeObject = {
                'id': noticeID,
                'title': noticeTitle,
                'link': noticeLink,
            }
        news['news'].append(noticeObject)

with codecs.open('news.json', 'w', 'utf-8') as outfile:
    json.dump(news, outfile, indent=2)
