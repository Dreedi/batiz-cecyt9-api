"""News model."""

from ferris3 import Model
from google.appengine.ext import ndb


class New(Model):

    """New Model."""

    title = ndb.StringProperty()
    url = ndb.StringProperty()
    image = ndb.StringProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)

    @classmethod
    def get_latests(self):
        """Get latests 20 News."""
        return self.query(projection=[self.title, self.url, self.image]).order(self.updated).fetch(20)
