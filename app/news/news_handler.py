"""News Handler."""

from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import json
from models import New
from urllib2 import urlopen
import webapp2


class NewsHandler(webapp2.RequestHandler):

    """News Handler."""

    def get(self):
        """Response to GET request."""
        news = New.get_latests()
        news_dict = {'news': []}
        for new in news:
            news_dict['news'].append(new.to_dict())
        self.response.headers.add_header("Access-Control-Allow-Origin", "*")
        self.response.headers['Content-Type'] = 'application/json'
        return self.response.write(json.dumps(news_dict))


class UpdateNewsHandler(webapp2.RequestHandler):

    """Update News Handler."""

    def get(self):
        """Update news."""
        url = 'http://www.cecyt9.ipn.mx/Paginas/inicio.aspx'
        html = urlopen(url).read()
        soup = BeautifulSoup(html, 'lxml')
        newsBox = soup.findAll('div', {'class': 'ca-item-main'})

        news = {'news': []}

        for idx, notice in enumerate(newsBox):
            if notice.find('div', {'class': 'ca-text'}):
                noticeTitle = notice.find('div', {'class': 'ca-text'}).find('span').text.replace('\t', '').replace('\r', '').replace('\n', '')
                noticeImage = notice.find('div', {'class': 'ca-icon'}).find('img')['src']
                if noticeTitle[0] == ' ':
                    noticeTitle = noticeTitle[1:]
                    noticeTitle = noticeTitle.capitalize()
                    noticeLink = notice.find('a', {'class': 'ca-more'})['href']
                    noticeObject = {
                        'title': noticeTitle.encode('UTF-8'),
                        'link': noticeLink,
                        'image': noticeImage,
                    }
                    query = New.query(New.title == noticeTitle and
                                      New.url == noticeLink and
                                      New.image == noticeImage).fetch()
                    if len(query) > 0:
                        query[0].updated = datetime.now()
                        query[0].put()
                    else:
                        news_object = New(title=noticeTitle, url=noticeLink, image=noticeImage)
                        news_object.put()
                    news['news'].append(noticeObject)
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(json.dumps(news))

webapp2_routes = [
    webapp2.Route('/news', NewsHandler),
    webapp2.Route('/news/', NewsHandler),
    webapp2.Route('/news/update', UpdateNewsHandler),
]
